#!/usr/bin/ruby
# vim: set sw=4 sts=4 et tw=80 :

require 'json'

class RepologyPage
    def initialize
        @ids = []
        @name = "packages"
        @dir = OutputDir
        @filename = @dir + @name + ".json"
    end

    def ensure_output_dir
        begin
            Pathname.new(@dir).mkpath
        rescue Errno::EEXIST
        end
    end

    def add_id id
        @ids.concat [id]
    end

    def generate_json
        '[' + @ids.map do | id |
            begin
                id.to_json()
            rescue Encoding::UndefinedConversionError
                $stderr << "Weird encoding in metadata for: " << id.name << "\n"
            end
        end.join(",") + ']'
    end

    def generate
        ensure_output_dir

        File.open(@filename, "w") do | file |
            file << generate_json
        end
    end
end
