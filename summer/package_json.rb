#!/usr/bin/ruby
# vim: set sw=4 sts=4 et tw=80 :

require 'json'

class RepologyPackage
    def initialize id, repo_sync_url
        @id = id
        @repo_sync_url = repo_sync_url
    end

    def metadata_key_to_string key
        case key
        when MetadataStringKey
            key.parse_value

        when MetadataStringSetKey, MetadataStringSequenceKey
            key.parse_value.join(', ')

        when MetadataSimpleURISpecTreeKey, MetadataDependencySpecTreeKey, MetadataFetchableURISpecTreeKey,
            MetadataPlainTextSpecTreeKey, MetadataLicenseSpecTreeKey
            result = []
            lambda do | recurse, value |
                case value
                when nil
                    # might've been normalised to nothing
                when AllDepSpec, AllDepSpec, ConditionalDepSpec
                    value.each do | child |
                        recurse.call(recurse, child)
                    end

                when SimpleURIDepSpec
                    result << value.text

                when FetchableURIDepSpec
                    result << value.original_url

                end
            end.tap do | x |
                x.call(x, key.parse_value)
            end
            result.join(" ")
        end
    end

    def metadata id
        ret = {}
        $stdout.flush
        id.each_metadata do | key |
            case key.raw_name
            when "HOMEPAGE"
                ret["homepage"] = metadata_key_to_string key
            when "DOWNLOADS"
                downloads = metadata_key_to_string key
                # basic check that there's some url scheme and not
                # the name of a manually downloaded file
                if downloads =~ /.+:\/\//
                    ret["downloads"] = downloads
                else
                    ret["downloads"] = ""
                end
            end
        end
        ret
    end

    def to_json

        if @repo_sync_url.include? "github.com"
          exheres_url = @repo_sync_url + "/blob/master/packages/" + @id.name.to_s + "/" + @id.name.package + "-" + @id.version.to_s + ".exheres-0"
          exheres_url_raw = @repo_sync_url.gsub("github.com", "raw.githubusercontent.com") + "/master/packages/" + @id.name.to_s + "/" + @id.name.package + "-" + @id.version.to_s + ".exheres-0"
        else
          exheres_url = @repo_sync_url + "/-/blob/master/packages/" + @id.name.to_s + "/" + @id.name.package + "-" + @id.version.to_s + ".exheres-0"
          exheres_url_raw = @repo_sync_url + "/-/raw/master/packages/" + @id.name.to_s + "/" + @id.name.package + "-" + @id.version.to_s + ".exheres-0"
        end

        m = {
            "name" => @id.name.package,
            "category" => @id.name.category,
            "version" => @id.version,
            "summary" => @id.short_description_key ?
                @id.short_description_key.parse_value.sub(/\.$/, '').force_encoding("UTF-8") :
                @id.name.package,
            "repository" => @id.repository_name,
            "exheres_url" => exheres_url,
            "exheres_url_raw" => exheres_url_raw
        }
        m.merge(metadata(@id)).to_json
    end
end
